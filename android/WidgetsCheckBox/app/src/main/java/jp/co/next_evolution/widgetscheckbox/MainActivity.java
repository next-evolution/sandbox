package jp.co.next_evolution.widgetscheckbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckBox chkMailSend = findViewById(R.id.chkMailSend);
        chkMailSend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String sResult;
                if( isChecked){
                    sResult = String.format("%s = ON", buttonView.getText());
                }else{
                    sResult = String.format("%s = OFF", buttonView.getText());
                }
                Toast.makeText(MainActivity.this, sResult, Toast.LENGTH_LONG).show();
            }
        });
    }
}
