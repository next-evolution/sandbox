package jp.co.next_evolution.networkjson;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class AsyncNetworkTask extends AsyncTask<String, Integer, String> {

    private TextView txtResult;
    private ProgressBar progressBar;

    public AsyncNetworkTask(Context context) {
        super();
        MainActivity activity = (MainActivity) context;
        this.txtResult = activity.findViewById(R.id.txtResult);
        this.progressBar = activity.findViewById(R.id.progressBar);
    }

    @Override
    protected String doInBackground(String... params) {
        publishProgress(30);
        SystemClock.sleep(5000);
        publishProgress(60);
        StringBuilder builder = new StringBuilder();
        StringBuilder list = new StringBuilder();

        try {
            URL url = new URL(params[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            try {
                JSONObject json = new JSONObject(builder.toString());
                JSONArray books = json.getJSONArray("books");
                for (int i = 0; i < books.length(); i++) {
                    JSONObject book = books.getJSONObject(i);
                    list.append(book.getString("title")).append(" / ");
                    list.append(book.getString("price")).append(System.lineSeparator());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        publishProgress(100);
        return list.toString();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.txtResult.setText(s);
        this.progressBar.setVisibility(ProgressBar.GONE);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        this.progressBar.setProgress(values[0]);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        this.txtResult.setText("CANCELしました。");
        this.progressBar.setVisibility(ProgressBar.GONE);
    }
}
