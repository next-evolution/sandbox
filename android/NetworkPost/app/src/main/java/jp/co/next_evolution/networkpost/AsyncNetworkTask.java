package jp.co.next_evolution.networkpost;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class AsyncNetworkTask extends AsyncTask<String, Integer, String> {

    private WeakReference<TextView> txtResult;
    private WeakReference<ProgressBar> progressBar;

    public AsyncNetworkTask(Context context) {
        super();
        MainActivity activity = (MainActivity) context;
        txtResult = new WeakReference<>((TextView) activity.findViewById(R.id.txtResult));
        progressBar = new WeakReference<>((ProgressBar) activity.findViewById(R.id.progressBar));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar.get().setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        publishProgress(30);
        SystemClock.sleep(5000);
        publishProgress(60);
        StringBuilder builder = new StringBuilder();
        try {
            URL url = new URL(params[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            conn.setDoOutput(true);

            OutputStream outputStream = conn.getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            printStream.print(params[1]);
            printStream.close();;

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append(System.lineSeparator());
            }

        } catch (Exception e) {
            builder.append(e);
        }
        publishProgress(100);
        return builder.toString();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Log.d("url", values[0].toString());
        progressBar.get().setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        txtResult.get().setText(s);
        progressBar.get().setVisibility(ProgressBar.GONE);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        txtResult.get().setText("CANCELされました。");
        progressBar.get().setVisibility(ProgressBar.GONE);
    }

}
