package jp.co.next_evolution.servicebroadcast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SimpleReceiver receiver = new SimpleReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimpleService.ACTION);
        registerReceiver(receiver,filter);
    }

    public void onStartClick(View view){
        Intent i = new Intent(this, jp.co.next_evolution.servicebroadcast.SimpleService.class);
        startService(i);
    }

    public void onStopClick(View view){
        Intent i = new Intent(this, jp.co.next_evolution.servicebroadcast.SimpleService.class);
        stopService(i);
    }
}
