package jp.co.next_evolution.servicetimer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onStartClick(View view) {
        Intent i = new Intent(this, jp.co.next_evolution.servicetimer.SimpleService.class);
        startService(i);
    }

    public void onStopClick(View view) {
        Intent i = new Intent(this, jp.co.next_evolution.servicetimer.SimpleService.class);
        stopService(i);
    }
}
