package jp.co.next_evolution.filebasic;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StringBuilder str = new StringBuilder();

        File f = new File(this.getFilesDir().getAbsolutePath() + "/" + "output.txt");
        if (f.exists()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(openFileInput("output.txt")))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line).append(System.getProperty("line.separator"));
                }
                ((EditText)findViewById(R.id.txtMemo)).setText(str.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(openFileOutput("output.txt", Context.MODE_PRIVATE)))) {
                    writer.write(((EditText) findViewById(R.id.txtMemo)).getText().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}