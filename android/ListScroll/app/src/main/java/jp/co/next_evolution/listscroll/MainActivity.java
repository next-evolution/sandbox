package jp.co.next_evolution.listscroll;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<String> data = new ArrayList<>();
        data.add("リスト０１");
        data.add("リスト０２");
        data.add("リスト０３");
        data.add("リスト０４");
        data.add("リスト０５");
        data.add("リスト０６");
        data.add("リスト０７");
        data.add("リスト０８");
        data.add("リスト０９");
        data.add("リスト１０");
        data.add("リスト１１");
        data.add("リスト１２");
        data.add("リスト１３");
        data.add("リスト１４");
        data.add("リスト１５");
        data.add("リスト１６");
        data.add("リスト１７");
        data.add("リスト１８");
        data.add("リスト１９");
        data.add("リスト２０");

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, data);
        ListView list = findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(totalItemCount < 25 ) {
                    if ((firstVisibleItem + visibleItemCount + 5) > totalItemCount) {
                        adapter.add("リスト２１_add");
                        adapter.add("リスト２２_add");
                        adapter.add("リスト２３_add");
                        adapter.add("リスト２４_add");
                        adapter.add("リスト２５_add");
                    }
                }
            }
        });
    }
}
