package jp.co.next_evolution.preferencebasic;


import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

public class MyConfigFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref);
    }
}
