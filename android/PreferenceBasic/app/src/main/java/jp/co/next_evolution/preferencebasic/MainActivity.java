package jp.co.next_evolution.preferencebasic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String msg = "";
        msg += "ユーザ名=" + pref.getString("edittext_name", "ゲスト");
        msg += "\nパスワード=" + pref.getString("edittext_pw", "123abc");
        msg += "\n年齢=" + pref.getString("edittext_age", "20");
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

        Button btnPreference = findViewById(R.id.btnPreference);
        btnPreference.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), MyConfigActivity.class);
                startActivity(i);
            }
        });
    }
}
