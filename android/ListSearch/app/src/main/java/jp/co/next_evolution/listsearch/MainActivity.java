package jp.co.next_evolution.listsearch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> data = new ArrayList<>();
        data.add("住宅ローン貸出（前月比）");
        data.add("失業率");
        data.add("就業者数");
        data.add("新築住宅価格指数（前月比）");
        data.add("貿易収支");
        data.add("消費者信頼感・速報値");
        data.add("マネーストックM3（前年比）");
        data.add("単位労働コスト・確報値（前期比）");
        data.add("企業景況感・製造業PMI");
        data.add("30年債入札");
        data.add("ミシガン大学消費者信頼感指数・確報値");
        data.add("ミシガン大学消費者信頼感指数・速報値");
        data.add("企業在庫（前月比）");
        data.add("単位労働コスト・確報値（前期比）");
        data.add("単位労働コスト・速報値（前期比）");
        data.add("卸売在庫・確報値（前月比）");
        data.add("卸売売上高（前月比）");
        data.add("失業率");
        data.add("平均時給（前月比）");
        data.add("新規失業保険申請件数");
        data.add("消費者物価指数・コア（前月比）");
        data.add("消費者物価指数（前月比）");
        data.add("製造業受注指数（前月比）");
        data.add("財政収支");
        data.add("貿易収支");
        data.add("輸入物価指数（前月比）");
        data.add("非農業部門労働生産性・確報値（前期比）");
        data.add("非農業部門労働生産性・速報値（前期比）");
        data.add("非農業部門雇用者数");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, data);
        final ListView list = findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setTextFilterEnabled(true);

        SearchView searchView = findViewById(R.id.search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText == null || newText.equals("")) {
                    list.clearTextFilter();
                } else {
                    list.setFilterText(newText);
                }
                return false;
            }
        });
    }
}
