package jp.co.next_evolution.widgetstogglebutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ToggleButton btnToggle = findViewById(R.id.btnToggle);
        btnToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Switch btnSwitch = findViewById(R.id.btnSwitch);
                btnSwitch.setChecked(isChecked);
                Toast.makeText(MainActivity.this, "TOGGLE=" + buttonView.getText(), Toast.LENGTH_LONG).show();
            }
        });

        Switch btnSwitch = findViewById(R.id.btnSwitch);
        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ToggleButton btnToggle = findViewById(R.id.btnToggle);
                btnToggle.setChecked(isChecked);
                Toast.makeText(MainActivity.this, "SWITCH=" + buttonView.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
