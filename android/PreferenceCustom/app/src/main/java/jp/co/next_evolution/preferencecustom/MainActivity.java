package jp.co.next_evolution.preferencecustom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String msg = "ユーザ名=" + preferences.getString("edittext_name", "guest");
        msg += "\n血液型=" + preferences.getString("list", "B型");
        msg += "\nNews購読=" + preferences.getBoolean("chk", true);

        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

        Button btnPreference = findViewById(R.id.btnPreference);
        btnPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MainActivity.this
                Intent i = new Intent(MainActivity.this, MyConfigActivity.class);
                startActivity(i);
            }
        });
    }
}
