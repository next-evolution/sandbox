package jp.co.next_evolution.databasebasic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SimpleDatabaseHelper extends SQLiteOpenHelper {

    private static final String DBNAME = "sample.sqlite";
    private static final int VERSION = 1;

    public SimpleDatabaseHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder();
        sql.append("create table books (")
                .append("isbn TEXT PRIMARY KEY")
                .append(", title TEXT")
                .append(", price INTEGER")
                .append(")");
        db.execSQL(sql.toString());

        db.execSQL("insert into books (isbn, title, price) VALUES('978-4-7980-4512-2', 'はじめてのASP.NET Webフォーム', 3000)");
        db.execSQL("insert into books (isbn, title, price) VALUES('978-4-7980-4179-7', 'ASP.NET MVC 5実践プログラミング', 3500)");
        db.execSQL("insert into books (isbn, title, price) VALUES('978-4-7741-8030-4', 'Javaポケットリファレンス ', 2680)");
        db.execSQL("insert into books (isbn, title, price) VALUES('978-4-7741-9617-6', 'Swiftポケットリファレンス', 2780)");
        db.execSQL("insert into books (isbn, title, price) VALUES('978-4-7981-3547-2', '独習PHP 第3版', 3200)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists books;");
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
