package jp.co.next_evolution.databasebasic;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private SimpleDatabaseHelper helper = null;
    private EditText txtIsbn = null;
    private EditText txtTitle = null;
    private EditText txtPrice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txtIsbn = findViewById(R.id.txtIsbn);
        this.txtTitle = findViewById(R.id.txtTitle);
        this.txtPrice = findViewById(R.id.txtPrice);
        helper = new SimpleDatabaseHelper(this);
//        try (SQLiteDatabase db = helper.getWritableDatabase()) {
//            Toast.makeText(this, "connected", Toast.LENGTH_LONG).show();
//        }catch (Exception e){
//            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
//        }

        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try (SQLiteDatabase db = helper.getWritableDatabase()) {
                    ContentValues cv = new ContentValues();
                    cv.put("isbn", txtIsbn.getText().toString());
                    cv.put("title", txtTitle.getText().toString());
                    cv.put("price", txtPrice.getText().toString());
                    db.insertWithOnConflict("books", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                    Toast.makeText(view.getContext(), "inserted.", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try (SQLiteDatabase db = helper.getWritableDatabase()) {
                    String[] params = {txtIsbn.getText().toString()};
                    db.delete("books", "isbn=?", params);
                    Toast.makeText(view.getContext(), "deleted.", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] cols = {"isbn", "title", "price"};
                String[] params = {txtIsbn.getText().toString()};
                try (SQLiteDatabase db = helper.getReadableDatabase();
                     Cursor cs = db.query("books", cols, "isbn=?", params, null, null, null)) {
                    if (cs.moveToFirst()) {
                        txtTitle.setText(cs.getString(1));
                        txtPrice.setText(cs.getString(2));
                    } else {
                        Toast.makeText(view.getContext(), "empty.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
