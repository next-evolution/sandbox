package jp.co.next_evolution;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView title;
    TextView tag;
    TextView desc;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        this.title = itemView.findViewById(R.id.title);
        this.tag = itemView.findViewById(R.id.tag);
        this.desc = itemView.findViewById(R.id.desc);
    }
}
