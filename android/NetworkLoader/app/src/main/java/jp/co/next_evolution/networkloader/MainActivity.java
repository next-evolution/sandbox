package jp.co.next_evolution.networkloader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    TextView txtResult;
    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtResult = findViewById(R.id.txtResult);
        spinner = findViewById(R.id.spinner);

        Bundle b = new Bundle();
        b.putString("url", "https://wings.msn.to/");
        LoaderManager.getInstance(this).initLoader(1, b, this);
    }

    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        if( id == 1 ){
            spinner.setVisibility(ProgressBar.VISIBLE);
            MyAsyncLoader loader = new MyAsyncLoader(this,args.getString("url"));
            loader.forceLoad();
            return loader;
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        this.txtResult.setText(data);
        spinner.setVisibility(ProgressBar.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }
}
