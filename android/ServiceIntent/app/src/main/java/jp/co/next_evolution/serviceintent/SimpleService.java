package jp.co.next_evolution.serviceintent;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

public class SimpleService extends IntentService {

    private final String TAG = "SimpleService";

    private int counter = 0;

    public SimpleService(String name) {
        super(name);
        Log.i(TAG, "constructor");
    }

    public SimpleService() {
        super("SimpleService");
        Log.i(TAG, "defaultConstructor");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        counter++;
        Log.i(TAG, "onHandleIntent-" + counter);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
