package jp.co.next_evolution.serviceintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        i = new Intent(this,jp.co.next_evolution.serviceintent.SimpleService.class);
    }

    public void onStartClick(View view) {
        startService(i);
    }
}
