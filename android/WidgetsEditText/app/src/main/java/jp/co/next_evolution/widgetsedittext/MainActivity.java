package jp.co.next_evolution.widgetsedittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText txtName = findViewById(R.id.txtName);
        txtName.setFocusable(true);

        Button btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText txtName = findViewById(R.id.txtName);
                TextView txtResult = findViewById(R.id.txtResult);
                txtResult.setText(String.format("こんにちは「 %s 」さん(^^)", txtName.getText()));
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TextView txtResult = findViewById(R.id.txtResult);
        outState.putString("txtResult", txtResult.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView txtResult = findViewById(R.id.txtResult);
        txtResult.setText(savedInstanceState.getString("txtResult"));
    }

}
