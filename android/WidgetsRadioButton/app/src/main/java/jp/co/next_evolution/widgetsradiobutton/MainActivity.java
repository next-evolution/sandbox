package jp.co.next_evolution.widgetsradiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RadioGroup rdoOS = findViewById(R.id.rdoOS);
        rdoOS.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton btnRadio = group.findViewById(checkedId);
                Toast.makeText(MainActivity.this, btnRadio.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
