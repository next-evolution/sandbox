package jp.co.next_evolution.sandbox.android.mysample;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        MainFragment fragment1 = new MainFragment();
//        MainFragment fragment2 = new MainFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, MainFragment.createInstance("hoge", Color.RED));
        transaction.add(R.id.container, MainFragment.createInstance("fuga", Color.BLUE));
//        transaction.add(R.id.container, fragment1);
//        transaction.add(R.id.container, fragment2);
//        transaction.add(R.id.mainActivity, fragment);
//        transaction.add(R.id.mainActivity, fragment);
        transaction.commit();
    }
}
