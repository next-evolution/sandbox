package jp.co.next_evolution.sandbox.android.mysample;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.CheckResult;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MainFragment extends Fragment {

    private static final String KEY_NAME = "key_name";
    private static final String KEY_BACKGROUND = "key_background_color";
    private TextView mTextView;
    private String mName = "";
    private @ColorInt
    int mBackGroundColor = Color.TRANSPARENT;

    @CheckResult
    public static MainFragment createInstance(String name, @ColorInt int color) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(KEY_NAME, name);
        args.putInt(KEY_BACKGROUND, color);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mName = args.getString(KEY_NAME);
            mBackGroundColor = args.getInt(KEY_BACKGROUND);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setBackgroundColor(mBackGroundColor);

        mTextView = view.findViewById(R.id.textView);
        mTextView.setText(mName);

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextView.setText(mTextView.getText() + "!");
            }
        });
    }
}
