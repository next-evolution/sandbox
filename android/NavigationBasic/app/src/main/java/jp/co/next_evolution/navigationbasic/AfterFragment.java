package jp.co.next_evolution.navigationbasic;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class AfterFragment extends Fragment {


    public AfterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_after, container, false);
        TextView txt = v.findViewById(R.id.txtValue);
        txt.setText("乱数：" + getArguments().getInt("num"));
        return v;
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_after, container, false);
    }

}
