package jp.co.next_evolution.navigationbasic;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class BeforeFragment extends Fragment {


    public BeforeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_before, container, false);
        view.findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("num", (new Random()).nextInt(100));
                Navigation.findNavController(v).navigate(R.id.afterFragment, bundle);
            }
        });
        return view;
    }

}
